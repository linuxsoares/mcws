package br.com.innv4.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by gilmar on 18/06/16.
 */


@RestController
public class AuthResource {

    @RequestMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }
}
