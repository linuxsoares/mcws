package br.com.innv4.resource;

import br.com.innv4.model.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by gilmar on 18/06/16.
 */

@RestController
public class UserResource {

    private ResponseEntity createUser(User user, UriComponentsBuilder ucBuilder){
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<User>(user, headers, HttpStatus.OK);
    }
}
